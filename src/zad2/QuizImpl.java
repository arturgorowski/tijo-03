public class QuizImpl implements Quiz {
    private int digit;
    public QuizImpl() {
        this.digit = 200; // w kazdej chwili zmienna moze otrzymac inna wartosc!
    }

    @Override
    public void isCorrectValue(int value) throws ParamTooLarge, ParamTooSmall {
        if(value <= MAX_VALUE && value >= MIN_VALUE) {
            if (digit > value) {
                throw new ParamTooLarge();
            } else if (digit < value){
                throw  new ParamTooSmall();
            }
        }

    }

    public int getDigit() {
        return digit;
    }

    public void setDigit(int digit) {
        this.digit = digit;
    }
}