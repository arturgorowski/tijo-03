public class Main {
    /**
     * metoda main powinna implementowac algorytm do jak najszybszego wyszukiwania wartosci
     * zmiennej digit z klasy QuizImpl (zakladamy ze programista nie zna zawartosci klasy QuizImpl).
     * Nalezy zalozyc, ze pole gigit w klasie QuizImpl moze w kazdej chwili ulec zmianie. Do wyszukiwania
     * odpowiedniej wartosci nalezy wykorzystywac tylko i wylacznie metode isCorrectValue - jesli metoda
     * przestanie rzucac wyjatki wowczas mamy pewnosc ze poszukiwana zmienna zostala odnaleziona.
     */
    public static void main(String[] args) {
        Quiz quiz = new QuizImpl();
        int digit = 254 ; // ??? <0, 1000> - minimalny i maksymalny zakres poszukiwan
        int MIN_VALUE = quiz.MIN_VALUE;
        int MAX_VALUE = quiz.MAX_VALUE;

        for(int counter = 1; ;counter++) {
            try {
                quiz.isCorrectValue(digit);
                System.out.println("Trafiona proba!!! Szukana liczba to: " + digit + " Ilosc prob: " + counter);
                break;
            } catch(Quiz.ParamTooLarge paramTooLarge) {
                System.out.println("Argument za duzy!!!");
                MAX_VALUE = quiz.getDigit();
                int max = (MIN_VALUE + quiz.getDigit()) / 2;
                quiz.setDigit(max);
            } catch(Quiz.ParamTooSmall paramTooSmall) {
                System.out.println("Argument za maly!!!");
                MIN_VALUE = quiz.getDigit();
                int min = (quiz.getDigit() + MAX_VALUE) / 2;
                quiz.setDigit(min);
            }
        }
    }
}