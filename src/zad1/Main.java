public class Main {

    public static void main(String[] args) {
        System.out.println();
        System.out.println("Generuje rozmiar wektora...");
        double[] vector = new double[Vector.generateSizeOfVector()];
        System.out.println("Wprowadzam dane do wektora...");
        Vector.insertDataToVector(vector);
        System.out.println("Wyswietlam zawartosc wektora");
        Vector.showVector(vector);
    }
}