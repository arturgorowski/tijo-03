public class Vector {
    /** funkcja zwraca wylosowany rozmiar wektora z zakresu <5, 10> */
    public static int generateSizeOfVector() {
        return (int) (Math.random() * 7) + 5;
    }
    /**
     * funkcja wprowadza dane do wektora. Podczas przetwarzania danych nie
     * mozna korzystac z informacji o rozmiarze wektora. Prosze odpowiednio
     * obsluzyc wyjatek ArrayIndexOutOfBoundsException.
     * Podczas kazdej iteracji losujemy liczbe typu double a nastepnie wprowadzamy
     * ja do naszego wektora - do momentu az przekroczymy rozmiar tablicy i wystapi
     * wyjatek ArrayIndexOutOfBoundsException, ktory odpowiednio obsluzymy.
     */
    public static void insertDataToVector(double[] vector) {
        int i = 0;
        boolean correct =true;
        while (correct) {
            try {
                vector[i] = Math.random();
            } catch (ArrayIndexOutOfBoundsException e) {
                correct = false;
                System.out.println("Znaleziono wyjątek ArrayIndexOutOfBoundsException");
                System.out.println("Koniec wprowadzania danych");
            }
            i++;
        }
        // implementacja metody...
    }
    /**
     * funkcja wyswietla zawartosc wektora w odpowiednim formacie. Podczas
     * przetwarzania danych nie mozna korzystac z
     * informacji o rozmiarze wektora. Prosze odpowiednio obsluzyc wyjatek
     * ArrayIndexOutOfBoundsException. Podczas kazdej iteracji wyswietlamy kolejny
     * rekord tablicy az przekroczymy jej rozmiar i wystapi wyjatek
     * ArrayIndexOutOfBoundsException, ktory odpowiednio obsluzymy.
     */
    public static void showVector(double[] vector) {
// implementacja metody...
        int i = 0;
        boolean correct =true;
        while (correct){
            try {
                System.out.println("TAB[" + i + "]: " + vector[i]) ;
            }catch (ArrayIndexOutOfBoundsException e){
                correct = false;
                System.out.println("Znaleziono wyjątek ArrayIndexOutOfBoundsException");
                System.out.println("Koniec wprowadzania danych!");
            }
            i++;
        }
    }
    /** funkcja, w ktorej bedziemy przeprowadzali testy nowych funkcjonalnosci. */

}